#!/usr/bin/env python3

import re
import os
import sys
import shutil
import requests
import transmissionrpc

TR_WEB = '172.16.88.58'
TR_PORT = 9092
ROOT = 'http://172.16.88.58:8082/downloads/'

def cleanup_title(filename):
    new = re.sub(r'\[.*?\]','', filename)
    new = re.sub(r'\{.*?\}','', new)
    new = re.sub(r'\(.*?\)','', new)
    new = re.sub(r'\s+',' ', new)
    new = re.sub(r'\s\.','.', new)
    new = new.strip()

    m = re.search('.+?(?=\s\-[\s\d+])', new)
    if m:
        dirname = str(m.group(0))
    else:
        dirname = ''

    return dirname + '/' + new

def get_dllist():
    dllist = []
    tc = transmissionrpc.Client(TR_WEB, port=TR_PORT)
    torrents = [ t for t in tc.get_torrents() 
            if t.status == 'stopped' or t.status == 'seeding' ]
    torrents.reverse()
    term_height = shutil.get_terminal_size()[1]
    i = 0
    for t in torrents[0:term_height-2]:
        if t.name.endswith(('mkv', 'mp4')) and t.name.startswith('['):
            print('{} - {}'.format(i, t.name))
            i += 1
            dllist.append((ROOT+t.name, ROOT+cleanup_title(t.name)))
    return dllist

def is_exist(url):
    r = requests.head(url, timeout=1.5)
    return r.status_code == requests.codes.ok

def get_choice():
    queue = []
    try:
        print('')
        print('Input index num, range is supported.')
        print('e.g. 0,2,3,5-8')
        choice = input('>>>> ')
        if ',' in choice and '-' in choice:
            choices = choice.split(',')
            for c in choices:
                if not '-' in c:
                    queue.append(int(c))
                else:
                    start = int(c.split('-')[0])
                    end = int(c.split('-')[1])
                    for i in range(start, end+1):
                        queue.append(i)
        elif ',' in choice:
            choices = choice.split(',')
            for c in choices:
                queue.append(int(c))
        elif '-' in choice:
            start = int(choice.split('-')[0])
            end = int(choice.split('-')[1])
            for i in range(start, end+1):
                queue.append(i)
        else:
            queue = [int(choice)]
    except ValueError:
        print('The fuck man')
        sys.exit(1)
    except KeyboardInterrupt:
        print('\nExiting')
        sys.exit(1)

    return queue


def main():
    os.system('clear')
    print('AYYY CARAMBA AMIGOMIO')
    print('---------------------')
    dllist = get_dllist()

    queue = get_choice()
    for q in queue:
        t1, t2 = dllist[q]
        if is_exist(t1):
            os.system('mpv \'{}\''.format(t1))
        elif is_exist(t2):
            os.system('mpv \'{}\''.format(t2))
        else:
            print('File gone man')

if __name__ == '__main__':
    main()

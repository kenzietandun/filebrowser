certifi==2017.11.5
chardet==3.0.4
idna==2.6
requests==2.18.4
six==1.11.0
transmissionrpc==0.11
urllib3==1.22
